package com.dimdarkevil.noitautil.model

import java.awt.Component
import javax.swing.JPanel

data class UiInfoComp(
	val name: String,
	val panel: JPanel,
	val comp: Component,
)
