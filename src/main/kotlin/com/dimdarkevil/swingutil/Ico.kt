package com.dimdarkevil.swingutil

enum class Ico(val fname: String) {
	BACKUP("backup.png"),
	EXIT("exit.png"),
	DELETE("delete.png"),
	LOG("log.png"),
	REFRESH("refresh.png"),
	RUN("run.png"),
	SETTINGS("settings.png"),
	RESTORE("restore.png"),
}